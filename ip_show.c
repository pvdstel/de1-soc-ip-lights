#include <arpa/inet.h>
#include <fcntl.h>
#include <ifaddrs.h>
#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <unistd.h>
#include "lib/socal.h"
#include "lib/hps.h"
#include "lib/hps_0.h"

struct ipv4addr
{
    int g1;
    int g2;
    int g3;
    int g4;
};

struct ipv4addr *get_ipv4(char *adapter);
int get_s7display_code(int n);

int open_physical(int);
void *map_physical(int, unsigned int, unsigned int);
void close_physical(int);
int unmap_physical(void *, unsigned int);

#define HW_REGS_BASE (ALT_STM_OFST)
#define HW_REGS_SPAN (0x04000000)
#define HW_REGS_MASK (HW_REGS_SPAN - 1)

int showIp(struct ipv4addr addr)
{
    int fd = -1;      // used to open /dev/mem
    void *LW_virtual; // physical addresses for light-weight bridge

    // Create virtual memory access to the FPGA light-weight bridge
    if ((fd = open_physical(fd)) == -1)
        return (-1);
    if ((LW_virtual = map_physical(fd, HW_REGS_BASE, HW_REGS_SPAN)) == NULL)
        return (-1);

    volatile unsigned long *h2p_lw_led_addr = LW_virtual + ((unsigned long)(ALT_LWFPGASLVS_OFST + LED_PIO_BASE) & (unsigned long)(HW_REGS_MASK));
    volatile unsigned long *h2p_lw_hex_addr = LW_virtual + ((unsigned long)(ALT_LWFPGASLVS_OFST + SEG7_IF_BASE) & (unsigned long)HW_REGS_MASK);

    alt_write_word(h2p_lw_led_addr, addr.g1 & 0xFF); //0:ligh, 1:unlight

    alt_write_word(h2p_lw_hex_addr, get_s7display_code(addr.g4 & 0xF));
    alt_write_word(h2p_lw_hex_addr + 1, get_s7display_code(addr.g4 >> 4 & 0xF));
    alt_write_word(h2p_lw_hex_addr + 2, get_s7display_code(addr.g3 & 0xF));
    alt_write_word(h2p_lw_hex_addr + 3, get_s7display_code(addr.g3 >> 4 & 0xF));
    alt_write_word(h2p_lw_hex_addr + 4, get_s7display_code(addr.g2 & 0xF));
    alt_write_word(h2p_lw_hex_addr + 5, get_s7display_code(addr.g2 >> 4 & 0xF));

    unmap_physical(LW_virtual, HW_REGS_SPAN);
    close_physical(fd);
    return 0;
}

int main(int argc, char *argv[])
{
    if (argc < 2)
    {
        printf("Specify a network adapter name.\n");
        exit(EXIT_FAILURE);
    }

    struct ipv4addr *address = get_ipv4(argv[1]);
    if (address != NULL)
    {
        showIp(*address);
        free(address);
    }
    else
    {
        printf("No IP address found.\n");
    }

    exit(EXIT_SUCCESS);
}

int get_s7display_code(int n)
{
    switch (n)
    {
    case 0x0:
        return 0b0111111;
    case 0x1:
        return 0b0000110;
    case 0x2:
        return 0b1011011;
    case 0x3:
        return 0b1001111;
    case 0x4:
        return 0b1100110;
    case 0x5:
        return 0b1101101;
    case 0x6:
        return 0b1111101;
    case 0x7:
        return 0b0000111;
    case 0x8:
        return 0b1111111;
    case 0x9:
        return 0b1101111;
    case 0xA:
        return 0b1110111;
    case 0xB:
        return 0b1111100;
    case 0xC:
        return 0b0111001;
    case 0xD:
        return 0b1011110;
    case 0xE:
        return 0b1111001;
    case 0xF:
        return 0b1110001;
    default:
        return 0x0;
    }
}

struct ipv4addr *
get_ipv4(char *adapter)
{
    struct ifaddrs *ifaddr;

    if (getifaddrs(&ifaddr) == -1)
    {
        perror("getifaddrs");
        return NULL;
    }

    for (struct ifaddrs *ifa = ifaddr; ifa != NULL; ifa = ifa->ifa_next)
    {
        if (ifa->ifa_addr == NULL || strcmp(ifa->ifa_name, adapter) != 0)
            continue;
        int family;

        family = ifa->ifa_addr->sa_family;

        if (family == AF_INET)
        {
            char host[NI_MAXHOST];
            int s = getnameinfo(ifa->ifa_addr,
                                sizeof(struct sockaddr_in),
                                host, NI_MAXHOST,
                                NULL, 0, NI_NUMERICHOST);

            if (s != 0)
            {
                printf("getnameinfo() failed: %s\n", gai_strerror(s));
                return NULL;
            }

            in_addr_t ip = inet_addr(host);

            struct ipv4addr *ip_data = malloc(sizeof(struct ipv4addr));
            ip_data->g1 = ip & 0xFF;
            ip_data->g2 = (ip >> 8) & 0xFF;
            ip_data->g3 = (ip >> 16) & 0xFF;
            ip_data->g4 = (ip >> 24);

            freeifaddrs(ifaddr);
            return ip_data;
        }
    }

    freeifaddrs(ifaddr);
    return NULL;
}

/* Open /dev/mem to give access to physical addresses */
int open_physical(int fd)
{
    if (fd == -1) // check if already open
        if ((fd = open("/dev/mem", (O_RDWR | O_SYNC))) == -1)
        {
            printf("ERROR: could not open \"/dev/mem\"...\n");
            return (-1);
        }
    return fd;
}

/* Close /dev/mem to give access to physical addresses */
void close_physical(int fd)
{
    close(fd);
}

/*
 * Establish a virtual address mapping for the physical addresses starting
 * at base, and extending by span bytes
 */
void *map_physical(int fd, unsigned int base, unsigned int span)
{
    void *virtual_base;
    // Get a mapping from physical addresses to virtual addresses
    virtual_base = mmap(NULL, span, (PROT_READ | PROT_WRITE), MAP_SHARED, fd, base);
    if (virtual_base == MAP_FAILED)
    {
        printf("ERROR: mmap() failed...\n");
        close(fd);
        return (NULL);
    }
    return virtual_base;
}

/* Close the previously-opened virtual address mapping */
int unmap_physical(void *virtual_base, unsigned int span)
{
    if (munmap(virtual_base, span) != 0)
    {
        printf("ERROR: munmap() failed...\n");
        return (-1);
    }
    return 0;
}
